package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		String passkey = passKey(password);
		try {
			conn = DBManager.getConnection();
			String sql = "select * from user where login_id=? and password=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, passkey);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}

	}

	public List<User> findAll() {
		Connection conn = null;
		ArrayList<User> userList = new ArrayList<>();
		try {
			conn = DBManager.getConnection();

			String sql = "select * from user where login_id!='admin' ";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
		return userList;
	}

	public ArrayList<User> findSearch(String loginIdP ,String nameP , String startDate, String endDate) {
		Connection conn = null;
		ArrayList<User> userList = new ArrayList<>();
		try {
			conn = DBManager.getConnection();

			String sql = "select * from user where login_id !='admin' ";

			if(!(loginIdP.equals(""))) {
				sql += "AND login_id = '" + loginIdP + "'";
			}
			if(!(nameP.equals(""))) {
				sql +="and name like '%"+nameP+"%'";
			}
			if(!(startDate.equals(""))) {
				sql +="and birth_date >= '"+startDate+"'";
			}
			if(!(endDate.equals(""))) {
				sql +="and birth_date <= '"+endDate+"'";
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
		return userList;
	}

	public int userAdd(String inputId, String inputPassword, String inputUserName, String inputBirthdate) {
		Connection conn = null;

		String passkey = passKey(inputPassword);
		try {
			conn = DBManager.getConnection();
			String sql = "insert into user (login_id,name, birth_date, password, create_date, update_date)values(?,?,?,?,now(),now())";
			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setString(1, inputId);
			stmt.setString(2, inputUserName);
			stmt.setString(3, inputBirthdate);
			stmt.setString(4, passkey);
			int result = stmt.executeUpdate();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}

			}

		}
	}

	public User findBySelectInfo(String loginId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "select * from user where login_id=? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id = rs.getInt("id");
			String loginId1 = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			User user = new User(id, loginId1, name, birthDate, password, createDate, updateDate);
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}

	}

	public List<User> findByName(String inputName) {
		Connection conn = null;
		ArrayList<User> userlist = new ArrayList<>();
		try {
			conn = DBManager.getConnection();
			String sql = "select * from user where name like ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, "%"+inputName+"%");
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userlist.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
		return userlist;
	}

	public List<User> findByBirthdate(String fromBirthdate, String toBirthdate) {
		Connection conn = null;
		ArrayList<User> userlist = new ArrayList<>();
		try {
			conn = DBManager.getConnection();
			String sql = "select * from user where birth_date between ? and ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, fromBirthdate);
			pstmt.setString(2, toBirthdate);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userlist.add(user);

			}

		} catch (

		SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
		return userlist;
	}

	public int userUpdate(String id, String password, String username, String birthdate) {
		Connection conn = null;
		int rs = 0;
		String passkey = passKey(password);
		try {
			conn = DBManager.getConnection();
			String sql = "update user set  name=? ,birth_date=? ,password=? ,update_date=now() where login_id=?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, username);
			pstmt.setString(2, birthdate);
			pstmt.setString(3, passkey);
			pstmt.setString(4, id);
			rs = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}

			}

		}
		return rs;
	}

	public int userUpdate(String id, String username, String birthdate) {
		Connection conn = null;
		int rs = 0;

		try {
			conn = DBManager.getConnection();
			String sql = "update user set  name=? ,birth_date=?  ,update_date=now() where login_id=?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, username);
			pstmt.setString(2, birthdate);
			pstmt.setString(3, id);
			rs = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}

			}

		}
		return rs;
	}


	public int userDelete(String selectId) {
		Connection conn = null;
		int rs = 0;
		try {
			conn = DBManager.getConnection();
			String sql = "delete from user where login_id=?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, selectId);
			rs = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}

			}

		}
		return rs;
	}
	public String passKey(String password) {

		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";
		String result =null;
		//ハッシュ生成処理
		byte[] bytes=null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

		 result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();

		}
		return result;
	}

}
