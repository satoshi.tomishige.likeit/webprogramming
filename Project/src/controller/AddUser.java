package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class AddUser
 */
@WebServlet("/AddUser")
public class AddUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("Logout");
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/add.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String inputId = request.getParameter("InputId");
		String inputPassword = request.getParameter("InputPassword");
		String inputUserName = request.getParameter("InputUserName");
		String inputBirthdate = request.getParameter("InputBirthdate");
		String confirmPassword = request.getParameter("confirmPassword");

		if (!(inputPassword.equals(confirmPassword))) {
			request.setAttribute("errMsgPass", "同じ文字を入力してください");
			doGet(request,response);
		} else {

			UserDao userdao = new UserDao();
			if (userdao.findBySelectInfo(inputId) != null) {
				request.setAttribute("errMsgId", "同じIDが存在します");
				doGet(request, response);
			} else {

				int result = userdao.userAdd(inputId, inputPassword, inputUserName, inputBirthdate);

				if (result == 1) {
					response.sendRedirect("UserList");
				} else {
					request.setAttribute("errMsg", "入力された内容は正しくありません");
					doGet(request,response);
				}
			}

		}

	}
}
