package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Update
 */
@WebServlet("/Update")
public class Update extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Update() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("Logout");
		} else {

			String selectId = request.getParameter("selectId");
			UserDao userDao = new UserDao();
			User user = userDao.findBySelectInfo(selectId);
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String selectId = request.getParameter("selectId");
		String updatePassword = request.getParameter("password");
		String confirmPassword = request.getParameter("conpassword");
		String updateUserName = request.getParameter("userName");
		String updateBirthdate = request.getParameter("birthdate");
		UserDao userDao = new UserDao();

		if (!(updatePassword.equals(confirmPassword))&& updatePassword != null) {
			request.setAttribute("errMsg", "同じ文字を入力してください");
			doGet(request, response);
		} else if (updatePassword != null) {

			int result = userDao.userUpdate(selectId, updatePassword, updateUserName, updateBirthdate);

			if (result == 1) {

				response.sendRedirect("UserList");

			} else {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				doGet(request, response);
			}
		} else {
			int result = userDao.userUpdate(selectId, updateUserName, updateBirthdate);
			if (result == 1) {

				response.sendRedirect("UserList");

			} else {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				doGet(request, response);
			}
		}

	}

}
