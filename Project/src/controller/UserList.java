package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserList
 */
@WebServlet("/UserList")
public class UserList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		if(session.getAttribute("userInfo")==null)  {response.sendRedirect("Logout");}else {
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();


		request.setAttribute("userList", userList);
		RequestDispatcher dispatcher =request.getRequestDispatcher("/WEB-INF/jsp/list.jsp");
		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("inputId");
		String userName = request.getParameter("inputUserName");
		String fromBirthdate = request.getParameter("fromBirthdate");
		String toBirthdate = request.getParameter("toBirthdate");

		UserDao userDao = new UserDao();
		ArrayList<User>user= new ArrayList<User>();

		user = userDao.findSearch(loginId,userName,fromBirthdate,toBirthdate);
//		List<User> userList1 = userDao.findByName(userName);
//		List<User> userList2 = userDao.findByBirthdate(fromBirthdate,toBirthdate);
//		ArrayList<User> userlist = new ArrayList<>();
//		if (user!=null) {userlist.add(user);}
//		if (userList1!=null) {userlist.addAll(userList1);}
//		if (userList2!=null) {userlist.addAll(userList2);}
//		ArrayList<User> fuserlist = new ArrayList<>(new HashSet<> (userlist));


		request.setAttribute("userList", user);

		RequestDispatcher dispatcher =request.getRequestDispatcher("/WEB-INF/jsp/list.jsp");
		dispatcher.forward(request, response);
	}

}
