<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<h4 align="right" class="menu_bar">
		<font color="white">${userInfo.name }</font> <a href="Logout"
			class="sample2">ログアウト</a>
	</h4>
	<div class="container">
		<h1 align="center">ユーザ新規登録</h1>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<form class="add-user" action="AddUser" method="post">
			<div>
				<div class="form-group">
					<label for="InputId">ログインID</label> <input type="text"
						class="form-control" name="InputId" id="InputId"
						placeholder="Enter ID">
				</div>
				<c:if test="${errMsgId != null}">
					<div class="alert alert-danger" role="alert">${errMsgId}</div>
				</c:if>

				<div class="form-group">
					<label for="InputPassword">パスワード</label> <input type="text"
						class="form-control" name="InputPassword" id="InputPassword"
						placeholder="Enter Password">
				</div>
				<div class="form-group">
					<label for="InputConfirmPassword">パスワード（確認）</label> <input type="text"
						class="form-control" name="confirmPassword" id="confirmPassword"
						placeholder="Enter Password">
				</div>
				<c:if test="${errMsgPass != null}">
					<div class="alert alert-danger" role="alert">${errMsgPass}</div>
				</c:if>
				<div class="form-group">
					<label for="InputUserName">ユーザ名</label> <input type="text"
						class="form-control" name="InputUserName" id="InputUserName"
						placeholder="Enter User Name">
				</div>
				<div class="form-group">
					<label for="InputBirthdate">生年月日</label> <input type="date"
						value="1994-02-10" class="form-control" name="InputBirthdate"
						id="InputBirthdate">
				</div>


				<br>
				<button class="btn btn-primary" type="submit">登録</button>
				<a class="btn btn-primary" href="UserList">戻る</a>
			</div>
		</form>
	</div>
</body>
</html>
