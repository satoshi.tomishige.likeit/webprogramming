<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>
	<br>
	<h1 align=center>ログイン画面</h1>
	<br>
	<!--
        <h2 align=center>ログインID:<input type="text"></h2>
        <h2 align=center>パスワード:<input type="password"></h2>

--><c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	<div class="container">
		<form class="form-signin" action="LoginServlet" method="post">
			<div class="form-group">
				<label for="exampleInputId">ログインID</label> <input type="text"
					class="form-control" id="exampleInputId" name="loginId"
					placeholder="Enter ID">
			</div>

			<div class="form-group">
				<label for="exampleInputPassword">パスワード</label> <input type="text"
					class="form-control" id="exampleInputPsaaword" name="password"
					placeholder="Enter Password">
			</div>

			<h2 align=center>
				<button class="btn btn-primary" role="button" type="submit">ログイン</button>
			</h2>

		</form>
	</div>
</body>
</html>