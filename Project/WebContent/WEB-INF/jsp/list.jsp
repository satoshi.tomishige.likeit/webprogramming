<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container-fluid">
	<h6 align="right" class="menu_bar">

			<font color="white">${userInfo.name }</font> <a href="Logout" class="sample2">ログアウト</a>
	</h6>
	</div>
	<div class="container">
		<h6 align="right">
			<c:if test="${userInfo.loginId =='admin'}"><a type="button" class="btn btn-secondary"
				href=AddUser> 新規登録</a></c:if>
		</h6>
		<h1 align="center">ユーザ一覧</h1>
		<br>


		<form class="search" action="UserList" method="post">
			<div class="form-group">
				<label for="InputId">ログインID</label> <input type="text"
					class="form-control" id="InputId" placeholder="Enter ID" name="inputId" value="">
			</div>
			<div class="form-group">
				<label for="InputUserName">ユーザ名</label> <input type="text"
					class="form-control" id="InputUserName" name="inputUserName"
					placeholder="Enter User Name" value="">
			</div>
			<div class="form-group">
				<label for="exampleInputBirthdate">生年月日</label> <input type="date"
				 class="form-control" id="FromBirthdate" name="fromBirthdate" value="">
				~ <input type="date"  class="form-control"
					id="ToBirthdate" name="toBirthdate" value="">

			</div>
			<button type="submit" class="btn btn-primary">検索</button>
		</form>
		<table class="table">
			<thead>
				<tr>
					<th scope="col">ログインID</th>
					<th scope="col">ユーザ名</th>
					<th scope="col">生年月日</th>
					<th scope="col"></th>
				</tr>
			</thead>

			<tbody>
				<c:forEach var="user" items="${userList}">

					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<td>
					<!--						<form  action="Inform">
 					 <input type="hidden" name="selectId" value=${user.loginId }>
  						<input type="submit" class="btn btn-primary" value="詳細">
							</form> -->
				<a href="Inform?selectId=${user.loginId }" class="btn btn-primary">詳細</a>
			<c:if test="${userInfo.loginId ==user.loginId or userInfo.loginId =='admin'}">	<a href="Update?selectId=${user.loginId }" class="btn btn-primary">更新</a></c:if>
			<c:if test="${userInfo.loginId =='admin'}">	<a href="Delete?selectId=${user.loginId }" class="btn btn-primary">削除</a></c:if>

						</td>

					</tr>

				</c:forEach>
			</tbody>
		</table>

	</div>

</body>
</html>
