<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<h4 align="right" class="menu_bar">
	<font color="white">${userInfo.name }</font> <a href="Logout" class="sample2">ログアウト</a>
	</h4>
	<div class="container">
		<h1 align="center">ユーザ削除確認</h1>

		<h4 align="center">
			ログインID:${selectId } <br>を本当に削除してもよろしいでしょうか。
		</h4>

		<h4 align="center">

			<form action="Delete" method="post">
				<input type="hidden" name="selectId" value=${selectId }>
				<input type="submit" class="btn btn-primary" value="OK">
			</form>
			<br>
			<a href="UserList" class="btn btn-primary" role="button">キャンセル</a>

		</h4>
	</div>

</body>
</html>
