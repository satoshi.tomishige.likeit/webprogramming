<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<h4 align="right" class="menu_bar">
	<font color="white">${userInfo.name }</font> <a href="Logout" class="sample2">ログアウト</a>
	</h4>
	<div class="container">
		<h1 align="center">ユーザ情報更新</h1>
		<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
		<form action="Update" method="post">
			<div class="form-group">
				<label for="InputId">ログインID : ${user.loginId }</label>
				<input type="hidden" name="selectId" value="${user.loginId }">

			</div>
			<div class="form-group">
				<label for="Password">新しいパスワード</label> <input type="password"
					class="form-control" id="Psaaword" name="password"
					placeholder="Enter Password" >
			</div>
			<div class="form-group">
				<label for="InputNameUser">新しいパスワード（確認）</label> <input type="password"
					class="form-control" id="conPassword" name="conpassword"
					placeholder="Enter Password" >
			</div>
			<div class="form-group">
				<label for="UserName">ユーザ名</label> <input type="text"
					class="form-control" id="UserName" name="userName"
					placeholder="Enter User Name" value="${user.name }">
			</div>
			<div class="form-group">
				<label for="Birthdate">生年月日</label> <input type="date"
					value="${user.birthDate }" class="form-control" id="Birthdate"
					name="birthdate"> <br>
				<h5 align="center">
					<button class="btn btn-outline-primary" type="submit">更新</button>
					<a class="btn btn-outline-primary" href="UserList">戻る</a>
				</h5>
			</div>
		</form>
	</div>
</body>
</html>
