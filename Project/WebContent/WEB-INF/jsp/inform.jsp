<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細参照</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
	<body>
        <h4 align="right" class="menu_bar">
          <font color="white">${userInfo.name }</font> <a href=Logout class="sample2">ログアウト</a>
            </h4>
        <div class="container">
		<h1 class="text-center">ユーザ情報詳細参照</h1>


            <table class="table table-striped">
  <thead>
    <tr>

      <th scope="col">ログインID</th>
      <th scope="col">${user.loginId }</th>

    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">ユーザ名</th>
      <td>${user.name}</td>

    </tr>
    <tr>
      <th scope="row">生年月日</th>
      <td>${user.birthDate }</td>

    </tr>
    <tr>
      <th scope="row">登録日時</th>
      <td>${user.createDate }</td>

    </tr>
       <tr>
      <th scope="row">更新日時</th>
      <td>${user.updateDate }</td>

    </tr>
  </tbody>
</table>
            <h4 align="center"><a href="UserList" class="btn btn-primary" role="button">戻る</a></h4>
        </div>
	</body>
</html>
